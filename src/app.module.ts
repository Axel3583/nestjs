import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostsModule } from './posts/posts.module';

@Module({
  imports: [
    PostsModule,
    MongooseModule.forRoot('mongodb+srv://axelyvesi2:GLZjtNkFax57Lk07@postcluster.tdheiab.mongodb.net/?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }),],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
