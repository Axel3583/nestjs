import { Body, Controller, Delete, Get, Param, Post as HttpPost, Put, NotFoundException, UseInterceptors } from '@nestjs/common';
import { PostsService } from './posts.service';
import { Post } from '../posts/entities/post.entity';
import { NotFoundInterceptor } from '../common/interceptors/not-found.interceptor';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) { }

  @HttpPost()
  async create(@Body() post: Post): Promise<Post> {
    return this.postsService.create(post);
  }

  @Get()
  async findAll(): Promise<Post[]> {
    return this.postsService.findAll();
  }

  @Get(':id')
  @UseInterceptors(new NotFoundInterceptor('Post not found'))
  async findOne(@Param('id') id: string): Promise<Post> {
    const post = await this.postsService.findOne(id);
    if (!post) throw new NotFoundException('Post not found');
    return post;
  }

  @Put(':id')
  @UseInterceptors(new NotFoundInterceptor('Post not found'))
  async update(@Param('id') id: string, @Body() post: Post): Promise<Post> {
    const updatedPost = await this.postsService.update(id, post);
    if (!updatedPost) throw new NotFoundException('Post not found');
    return updatedPost;
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<Post> {
    return this.postsService.delete(id);
  }
}
